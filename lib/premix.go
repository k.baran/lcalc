package lib

import (
	"fmt"
)

type shot struct {
	strength int
}

type premix struct {
	strength int
	capacity int
}

func Premix() (int, int) {

	prem := premix{0, 0}

	fmt.Printf("Quantity of pure premix [ml]: ")
	fmt.Scan(&prem.capacity)

	var shotsNumber int
	fmt.Printf("Number of shots: ")
	fmt.Scan(&shotsNumber)

	if shotsNumber <= 0 {
		println("Shots number invalid or equal 0")
		return 0, 0
	}

	var shots []shot

	for i := 1; i <= shotsNumber; i++ {
		var shot shot

		fmt.Printf("%d. shot power [mg/ml]: ", i)
		fmt.Scan(&shot.strength)

		shots = append(shots, shot)
	}

	var shotStrengthSum int

	for _, s := range shots {
		shotStrengthSum += s.strength
	}

	var outputCapacity = prem.capacity + 10*shotsNumber
	var outputStrength = shotStrengthSum * 10 / outputCapacity

	fmt.Printf("Output: %d ml of %d mg/ml eJuice \n", outputCapacity, outputStrength)

	return outputStrength, outputCapacity

}
